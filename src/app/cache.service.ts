import {Injectable, OnDestroy, OnInit, signal} from '@angular/core';
import * as moment from 'moment';
import {Store} from '@ngrx/store';
import {setCachedUrls} from './ngrx/actions.ngrx';
import {selectCacheDuration, selectCacheUrls} from './ngrx/selectors.ngrx';
import {Subscription} from 'rxjs';

export interface CachedUrl {
    url: string;
    result: string;
    date: number;
}

@Injectable()
export class CacheService {

    cachedUrls = signal<CachedUrl[]>([]);
    cacheDuration = signal(120);

    constructor(private store: Store) {
        this.store.select(selectCacheUrls).subscribe((cachedUrls: CachedUrl[]) =>
            this.cachedUrls.update(() => cachedUrls)
        );
        this.store.select(selectCacheDuration).subscribe((cacheDuration: number) =>
            this.cacheDuration.update(() => cacheDuration)
        );
    }

    getResult<T>(url: string): T {
        const cachedUrl: CachedUrl = this.cachedUrls().find((cachedUrl: CachedUrl) => cachedUrl.url === url);
        if (!cachedUrl) {
            return null;
        }
        const currentDate: number = moment().valueOf();
        const cachedDate: number = cachedUrl.date;
        return (currentDate - cachedDate < (this.cacheDuration() * 1000)) ? JSON.parse(cachedUrl.result) : null
    }

    setResult<T>(url: string, result: T): void {
        this.cachedUrls.update((cachedUrls: CachedUrl[]) => {
            const cachedUrl = {
                    url: url,
                    result: JSON.stringify(result),
                    date: moment().valueOf()
                }
            ;
            const filterOutUrl: CachedUrl[] = cachedUrls.filter((item: CachedUrl) => item.url !== url);
            localStorage.setItem('CACHE', JSON.stringify([
                ...filterOutUrl,
                cachedUrl
            ]));
            return [
                ...filterOutUrl,
                cachedUrl
            ]
        })
    }

}
