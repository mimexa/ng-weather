import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {getCurrentConditionsByZip, setCachedUrls, setLocations} from './ngrx/actions.ngrx';
import {LOCATIONS} from './location.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

    constructor(private store: Store) {
    }

    ngOnInit(): void {
        let locString: string = localStorage.getItem(LOCATIONS);
        if (!!locString) {
            const locations: string[] = JSON.parse(locString);
            this.store.dispatch(setLocations({locations}));
            locations.forEach((location: string) => {
                this.store.dispatch(getCurrentConditionsByZip({location, updateCache: false}))
            })
        }
        const cache: string = localStorage.getItem('CACHE');
        if (cache) {
            this.store.dispatch(setCachedUrls(JSON.parse(cache)));
        }
    }


}
