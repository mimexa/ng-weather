import {Injectable, signal} from '@angular/core';
import {Store} from '@ngrx/store';
import {selectLocations} from './ngrx/selectors.ngrx';

export const LOCATIONS: string = 'locations';

@Injectable()
export class LocationService {

    locations = signal<string[]>([]);

    constructor(private store: Store) {
        this.store.select(selectLocations).subscribe((locations: string[]) =>
            this.locations.update(() => locations)
        );
    }

    addLocation(zipcode: string, updateCache: boolean) {
        this.locations.update(locations => [...locations, zipcode]);
        if (updateCache) {
            localStorage.setItem(LOCATIONS, JSON.stringify(this.locations()));
        }
    }

    removeLocation(zipcode: string) {
        let locString: string = localStorage.getItem(LOCATIONS);
        if (!!locString) {
            const locations: string[] = JSON.parse(locString);
            localStorage.setItem(LOCATIONS, JSON.stringify(
                locations.filter((location: string) => location !== zipcode)
            ));
        }
    }

}
