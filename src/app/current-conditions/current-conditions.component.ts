import {Component, OnDestroy, signal} from '@angular/core';
import {ConditionsAndZip} from '../conditions-and-zip.type';
import {TabTitle} from '../modules/infinite-tabs/components/infinite-tabs/infinite-tabs.component';
import {Store} from '@ngrx/store';
import {selectConditions} from '../ngrx/selectors.ngrx';
import {removeLocation} from '../ngrx/actions.ngrx';
import {Subscription} from 'rxjs';
import {LocationService} from '../location.service';

@Component({
  selector: 'app-current-conditions',
  templateUrl: './current-conditions.component.html',
  styleUrls: ['./current-conditions.component.css']
})
export class CurrentConditionsComponent implements OnDestroy {

  protected conditions = signal<ConditionsAndZip[]>([]);
  protected tabTitles = signal<TabTitle[]>([]);
  private subs: Subscription[] = [];

  constructor(
    private store: Store,
    private locationService: LocationService
  ) {
    this.subs.push(this.store.select(selectConditions)
      .subscribe((conditions: ConditionsAndZip[]) => {
        this.tabTitles.update(() =>
          conditions.map((condition: ConditionsAndZip) => ({
            zip: condition.zip,
            name: condition.data.name
          }))
        );
        this.conditions.update(() => [...conditions]);
      }));
  }

  closeTabEvent(tabTitle: TabTitle) {
    this.store.dispatch(removeLocation({location: tabTitle.zip}));
    this.locationService.removeLocation(tabTitle.zip);
  }

  ngOnDestroy(): void {
    this.subs.forEach((sub: Subscription) => sub.unsubscribe());
  }

}
