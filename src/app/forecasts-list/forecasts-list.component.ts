import {Component, OnDestroy, OnInit, signal} from '@angular/core';
import {WeatherService} from '../weather.service';
import {ActivatedRoute} from '@angular/router';
import {Forecast} from './forecast.type';
import {Store} from '@ngrx/store';
import {Subscription} from 'rxjs';
import {getForecast} from '../ngrx/actions.ngrx';
import {selectForecast} from '../ngrx/selectors.ngrx';

@Component({
    selector: 'app-forecasts-list',
    templateUrl: './forecasts-list.component.html',
    styleUrls: ['./forecasts-list.component.css']
})
export class ForecastsListComponent implements OnInit, OnDestroy {

    zipcode: string;
    forecast = signal<Forecast>(null);
    subs: Subscription[] = [];

    constructor(
        private store: Store,
        private route: ActivatedRoute,
        protected weatherService: WeatherService
    ) {
    }

    ngOnInit(): void {
        this.subs.push(this.route.params.subscribe(params => {
            this.zipcode = params['zipcode'];
            this.store.dispatch(getForecast({location: this.zipcode}))
        }));
        this.subs.push(this.store.select(selectForecast).subscribe((forecast: Forecast) =>
            this.forecast.update(() => forecast)
        ));
    }

    ngOnDestroy(): void {
        this.subs.forEach((sub: Subscription) => sub.unsubscribe());
    }

}
