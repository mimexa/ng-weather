import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
import {ZipcodeEntryComponent} from './zipcode-entry/zipcode-entry.component';
import {LocationService} from './location.service';
import {ForecastsListComponent} from './forecasts-list/forecasts-list.component';
import {WeatherService} from './weather.service';
import {CurrentConditionsComponent} from './current-conditions/current-conditions.component';
import {MainPageComponent} from './main-page/main-page.component';
import {RouterModule} from '@angular/router';
import {routing} from './app.routing';
import {HttpClientModule} from '@angular/common/http';
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../environments/environment';
import {CurrentConditionComponent} from './current-condition/current-condition.component';
import {CachedHttpClientService} from './cached-http-client.service';
import {CacheService} from './cache.service';
import {StoreModule} from '@ngrx/store';
import {weatherReducer, WeatherState} from './ngrx/reducers.ngrx';
import {EffectsModule} from '@ngrx/effects';
import {WeatherEffects} from './ngrx/effects.ngrx';
import {InfiniteTabsModule} from './modules/infinite-tabs/infinite-tabs.module';
import {infiniteTabsReducer, InfiniteTabsState} from './modules/infinite-tabs/ngrx/reducers.ngrx';

export interface AppState {
    weather: WeatherState,
    infiniteTabs: InfiniteTabsState
}

@NgModule({
    declarations: [
        AppComponent,
        ZipcodeEntryComponent,
        ForecastsListComponent,
        CurrentConditionsComponent,
        MainPageComponent,
        CurrentConditionComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        RouterModule,
        StoreModule.forRoot<AppState>({
            weather: weatherReducer,
            infiniteTabs: infiniteTabsReducer
        }),
        routing,
        ServiceWorkerModule.register('/ngsw-worker.js', {enabled: environment.production}),
        EffectsModule.forRoot(WeatherEffects),
        ReactiveFormsModule,
        InfiniteTabsModule
    ],
    providers: [
        LocationService,
        WeatherService,
        CachedHttpClientService,
        CacheService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
