import {Component, Input} from '@angular/core';
import {ConditionsAndZip} from '../conditions-and-zip.type';
import {WeatherService} from '../weather.service';

@Component({
  selector: 'app-current-condition',
  templateUrl: './current-condition.component.html'
})
export class CurrentConditionComponent {

  @Input({required: true})
  condition: ConditionsAndZip;

  constructor(protected weatherService: WeatherService) {
  }

}
