import {
    getCurrentConditionsByZip,
    getCurrentConditionsByZipFailed,
    getCurrentConditionsByZipSuccess,
    getForecast,
    getForecastSuccess,
} from './actions.ngrx';
import {WeatherService} from '../weather.service';
import {catchError, concatMap, map, switchMap} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {ConditionsAndZip} from '../conditions-and-zip.type';
import {LocationService} from '../location.service';

@Injectable()
export class WeatherEffects {

    constructor(
        private weatherService: WeatherService,
        private actions$: Actions,
        private locationService: LocationService
    ) {
    }

    getCurrentConditionsByZip$ = createEffect(() =>
        this.actions$.pipe(
            ofType(getCurrentConditionsByZip),
            concatMap(payload => this.weatherService.getCurrentConditionsByZip(payload.location)
                .pipe(
                    switchMap((conditions: ConditionsAndZip) => {
                        this.locationService.addLocation(conditions.zip, payload.updateCache);
                        return [
                            getCurrentConditionsByZipSuccess(conditions)
                        ]
                    }),
                    catchError((err) => {
                        alert(`Unable to fetch weather data for given zip code ${payload.location}`)
                        return [
                            getCurrentConditionsByZipFailed()
                        ]
                    })
                )
            ),
        )
    );

    getForecast$ = createEffect(() =>
        this.actions$.pipe(
            ofType(getForecast),
            switchMap(payload => this.weatherService.getForecast(payload.location)
                .pipe(
                    map(forecast => getForecastSuccess(forecast)),
                )
            ),
        )
    );

}
