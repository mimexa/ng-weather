import {createAction, props} from '@ngrx/store';
import {ConditionsAndZip} from '../conditions-and-zip.type';
import {Forecast} from '../forecasts-list/forecast.type';
import {CachedUrl} from '../cache.service';

export const getCurrentConditionsByZip = createAction('[App] Get current conditions by zip', props<{
    location: string,
    updateCache: boolean
}>());
export const getCurrentConditionsByZipSuccess = createAction('[App] Get current conditions by zip success', props<
    ConditionsAndZip
>());
export const getCurrentConditionsByZipFailed = createAction('[App] Get current conditions by zip fail');
export const setLocations = createAction('[App] Set locations', props<{ locations: string[] }>());
export const removeLocation = createAction('[App] Remove location', props<{ location: string }>());
export const setCacheDuration = createAction('[App] Set cache duration', props<{ cacheDuration: number }>());
export const getForecast = createAction('[App] Get forecast', props<{ location: string }>());
export const getForecastSuccess = createAction('[App] Get forecast success', props<Forecast>());
export const getForecastFailed = createAction('[App] Get forecast failed');
export const setCachedUrls = createAction('[App] Set cached urls', props<CachedUrl>());

