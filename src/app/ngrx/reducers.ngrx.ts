import {ConditionsAndZip} from '../conditions-and-zip.type';
import {createReducer, on} from '@ngrx/store';
import {Forecast} from '../forecasts-list/forecast.type';
import {
    getCurrentConditionsByZip,
    getCurrentConditionsByZipFailed,
    getCurrentConditionsByZipSuccess,
    getForecast,
    getForecastFailed,
    getForecastSuccess,
    removeLocation,
    setCacheDuration,
    setLocations,
} from './actions.ngrx';
import {CachedUrl} from '../cache.service';

export interface WeatherState {
    conditions: ConditionsAndZip[];
    isGetCurrentConditionsByZipLoading: boolean;
    locations: string[];
    cacheDuration: number;
    forecast: Forecast;
    isGetForecastLoading: boolean;
    cacheUrls: CachedUrl[];
}

export const initialState: WeatherState = {
    conditions: [],
    isGetCurrentConditionsByZipLoading: false,
    locations: [],
    cacheDuration: 120,
    forecast: null,
    isGetForecastLoading: false,
    cacheUrls: []
};

export const weatherReducer = createReducer(
    initialState,
    on(getCurrentConditionsByZip, (state) => ({
        ...state,
        isGetCurrentConditionsByZipLoading: true
    })),
    on(getCurrentConditionsByZipSuccess, (state, conditions) => ({
        ...state,
        locations: [...state.locations, conditions.zip],
        conditions: [...state.conditions, conditions],
        isGetCurrentConditionsByZipLoading: false
    })),
    on(getCurrentConditionsByZipFailed, (state) => ({
        ...state,
        isGetCurrentConditionsByZipLoading: false
    })),
    on(removeLocation, (state, payload) => ({
        ...state,
        locations: [...state.locations.filter(loc => loc !== payload.location)],
        conditions: [...state.conditions.filter(conditions => conditions.zip !== payload.location)]
    })),
    on(setCacheDuration, (state, payload) => {
        alert(`The cache duration has been successfully set to ${payload.cacheDuration}`);
        return {
            ...state,
            cacheDuration: payload.cacheDuration
        }
    }),
    on(getForecast, (state) => ({
        ...state,
        isGetForecastLoading: true
    })),
    on(getForecastSuccess, (state, forecast) => ({
        ...state,
        forecast,
        isGetForecastLoading: false
    })),
    on(getForecastFailed, (state) => ({
        ...state,
        isGetForecastLoading: false
    })),
    on(setLocations, (state, payload) => ({
        ...state,
        locations: payload.locations
    }))
);
