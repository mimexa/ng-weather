import {WeatherState} from './reducers.ngrx';
import {createSelector} from '@ngrx/store';
import {AppState} from '../app.module';

export const selectWeatherState = (state: AppState) => state.weather;

export const selectConditions = createSelector(
    selectWeatherState,
    (state: WeatherState) => state.conditions
);
export const selectLocations = createSelector(
    selectWeatherState,
    (state: WeatherState) => state.locations
);

export const selectCacheDuration = createSelector(
    selectWeatherState,
    (state: WeatherState) => state.cacheDuration
);

export const selectIsGetCurrentConditionsByZipLoading = createSelector(
    selectWeatherState,
    (state: WeatherState) => state.isGetCurrentConditionsByZipLoading
);

export const selectCacheUrls = createSelector(
    selectWeatherState,
    (state: WeatherState) => state.cacheUrls
);

export const selectForecast = createSelector(
    selectWeatherState,
    (state: WeatherState) => state.forecast
);
