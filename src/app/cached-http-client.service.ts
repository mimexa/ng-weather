import {Injectable, signal, Signal} from '@angular/core';
import {WeatherService} from './weather.service';
import {HttpClient, HttpContext, HttpEvent, HttpHeaders, HttpParams, HttpResponse} from '@angular/common/http';
import {CacheService} from './cache.service';
import {Observable, of} from 'rxjs';
import {catchError, take, tap} from 'rxjs/operators';
import {Clouds, Coord, CurrentConditions, Main, Sys, Weather, Wind} from './current-conditions/current-conditions.type';

@Injectable()
export class CachedHttpClientService {

    constructor(
        private httpClient: HttpClient,
        private cacheService: CacheService
    ) {
    }

    public get<T>(url: string, options?: {
        headers?: HttpHeaders | {
            [header: string]: string | string[];
        };
        context?: HttpContext;
        observe?: 'body';
        params?: HttpParams | {
            [param: string]: string | number | boolean | ReadonlyArray<string | number | boolean>;
        };
        reportProgress?: boolean;
        responseType?: 'json';
        withCredentials?: boolean;
        transferCache?: {
            includeHeaders?: string[];
        } | boolean;
    }): Observable<any> {
        const result: T = this.cacheService.getResult<T>(url);
        return !!result ? of(result) : this.httpClient.get<T>(url, options).pipe(
            tap((result: T) => {
                this.cacheService.setResult(url, result);
            }),
        );
    }

}
