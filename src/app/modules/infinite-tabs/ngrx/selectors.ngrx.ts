import {InfiniteTabsState} from './reducers.ngrx';
import {createSelector} from '@ngrx/store';
import {AppState} from '../../../app.module';

export const selectInfiniteTabsState = (state: AppState) => state.infiniteTabs;

export const selectTabIndex = createSelector(
    selectInfiniteTabsState,
    (state: InfiniteTabsState) => state.tabIndexNumber
);

export const selectNbTabs = createSelector(
    selectInfiniteTabsState,
    (state: InfiniteTabsState) => state.nbTabs
);
