import {createReducer, on} from '@ngrx/store';
import {setNbTabs, setTabIndex} from './actions.ngrx';

export interface InfiniteTabsState {
    tabIndexNumber: number;
    nbTabs: number;
}

export const initialState: InfiniteTabsState = {
    tabIndexNumber: 0,
    nbTabs: 0
};

export const infiniteTabsReducer = createReducer(
    initialState,
    on(setTabIndex, (state, payload) => ({
        ...state,
        tabIndexNumber: payload.tabIndexNumber
    })),
    on(setNbTabs, (state, payload) => ({
        ...state,
        nbTabs: payload.nbTabs
    }))
);
