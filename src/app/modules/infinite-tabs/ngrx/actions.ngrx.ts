import {createAction, props} from '@ngrx/store';

export const setTabIndex = createAction('[Infinite tabs] Set tab index', props<{ tabIndexNumber: number }>());
export const setNbTabs = createAction('[Infinite tabs] Set number tabs', props<{ nbTabs: number }>());
