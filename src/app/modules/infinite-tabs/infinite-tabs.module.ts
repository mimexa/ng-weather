import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {InfiniteTabsComponent} from './components/infinite-tabs/infinite-tabs.component';
import {InfiniteTabsTitleComponent} from './components/infinite-tabs-title/infinite-tabs-title.component';
import {InfiniteTabsContentComponent} from './components/infinite-tabs-content/infinite-tabs-content.component';
import {InfiniteTabsTitlesComponent} from './components/infinite-tabs-titles/infinite-tabs-titles.component';
import {InfiniteTabsContentsComponent} from './components/infinite-tabs-contents/infinite-tabs-contents.component';

@NgModule({
    declarations: [
        InfiniteTabsComponent,
        InfiniteTabsTitleComponent,
        InfiniteTabsContentComponent,
        InfiniteTabsTitlesComponent,
        InfiniteTabsContentsComponent
    ],
    imports: [
        BrowserModule,
    ],
    exports: [
        InfiniteTabsComponent,
        InfiniteTabsTitleComponent,
        InfiniteTabsContentComponent,
        InfiniteTabsTitlesComponent,
        InfiniteTabsContentsComponent
    ],
    providers: []
})
export class InfiniteTabsModule {
}
