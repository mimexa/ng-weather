import {Component} from '@angular/core';

export interface TabTitle {
    zip: string;
    name: string;
}

@Component({
    selector: 'app-infinite-tabs',
    templateUrl: './infinite-tabs.component.html',
    styleUrls: ['./infinite-tabs.component.css'],
})
export class InfiniteTabsComponent {
}
