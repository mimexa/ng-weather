import {Component, ContentChildren, OnInit, QueryList} from '@angular/core';
import {InfiniteTabsTitleComponent} from '../infinite-tabs-title/infinite-tabs-title.component';
import {Store} from '@ngrx/store';
import {setTabIndex} from '../../ngrx/actions.ngrx';

@Component({
    selector: 'app-infinite-tabs-titles',
    templateUrl: './infinite-tabs-titles.component.html',
    styleUrls: ['./infinite-tabs-titles.component.css'],
})
export class InfiniteTabsTitlesComponent implements OnInit {

    @ContentChildren(InfiniteTabsTitleComponent)
    public infiniteTabsTitleComponents: QueryList<InfiniteTabsTitleComponent>;

    constructor(private store: Store) {
    }

    ngOnInit(): void {
        this.store.dispatch(setTabIndex({tabIndexNumber: 0}));
    }

}


