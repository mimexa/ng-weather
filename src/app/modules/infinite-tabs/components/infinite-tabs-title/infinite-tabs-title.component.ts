import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {setTabIndex} from '../../ngrx/actions.ngrx';
import {selectNbTabs, selectTabIndex} from '../../ngrx/selectors.ngrx';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-infinite-tabs-title',
    templateUrl: './infinite-tabs-title.component.html',
    styleUrls: ['./infinite-tabs-title.component.css'],
})
export class InfiniteTabsTitleComponent implements OnInit, OnDestroy {

    _tabIndexNumber: number = 0;
    protected selectedTab: number = 0;
    subs: Subscription[] = [];
    nbTabs: number = 0;

    constructor(private store: Store) {
    }

    @Input()
    set tabIndexNumber(tabIndexNumber: number) {
        setTimeout(() => this.store.dispatch(setTabIndex({tabIndexNumber: tabIndexNumber})));
        this._tabIndexNumber = tabIndexNumber;
    }

    ngOnInit(): void {
        this.subs.push(this.store.select(selectTabIndex).subscribe((tabIndex: number) => {
            this.selectedTab = tabIndex;
        }));
        this.subs.push(this.store.select(selectNbTabs).subscribe((nbTabs: number) => {
            this.nbTabs = nbTabs;
        }));
    }


    openTab() {
        this.store.dispatch(setTabIndex({tabIndexNumber: this._tabIndexNumber}));
    }

    ngOnDestroy(): void {
        this.subs.forEach((sub: Subscription) => sub.unsubscribe());
    }

}


