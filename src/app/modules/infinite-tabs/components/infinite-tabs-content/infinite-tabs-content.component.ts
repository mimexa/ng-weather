import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {selectNbTabs, selectTabIndex} from '../../ngrx/selectors.ngrx';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-infinite-tabs-content',
    templateUrl: './infinite-tabs-content.component.html',
    styleUrls: ['./infinite-tabs-content.component.css'],
})
export class InfiniteTabsContentComponent implements OnInit, OnDestroy {

    @Input()
    tabIndexNumber: number = 0;
    selectedTabIndex: number = 0;
    nbTabs: number = 0;
    subs: Subscription[] = [];

    constructor(private store: Store) {
    }

    ngOnInit(): void {
        this.subs.push(this.store.select(selectTabIndex).subscribe((tabIndex: number) => {
            this.selectedTabIndex = tabIndex;
        }));
        this.subs.push(this.store.select(selectNbTabs).subscribe((nbTabs: number) => {
            this.nbTabs = nbTabs;
        }));
    }

    ngOnDestroy(): void {
        this.subs.forEach((sub: Subscription) => sub.unsubscribe);
    }

}


