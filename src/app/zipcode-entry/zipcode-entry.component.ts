import {Component, OnDestroy, OnInit, signal} from '@angular/core';
import {Store} from '@ngrx/store';
import {selectIsGetCurrentConditionsByZipLoading, selectLocations} from '../ngrx/selectors.ngrx';
import {getCurrentConditionsByZip, setCacheDuration} from '../ngrx/actions.ngrx';
import {Subscription} from 'rxjs';
import {take} from 'rxjs/operators';
import {FormControl, Validators} from '@angular/forms';

@Component({
    selector: 'app-zipcode-entry',
    templateUrl: './zipcode-entry.component.html'
})
export class ZipcodeEntryComponent implements OnDestroy, OnInit {

    subs: Subscription[] = [];
    isGetCurrentConditionsByZipLoading = signal(true);
    zipCode = new FormControl('', [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(5)
    ]);
    duration = new FormControl(120, [
        Validators.required,
        Validators.min(1),
        Validators.max(24 * 60 * 60)
    ]);

    constructor(private store: Store) {
    }

    ngOnInit(): void {
        this.subs.push(this.store.select(selectIsGetCurrentConditionsByZipLoading).subscribe((isGetCurrentConditionsByZipLoading: boolean) => {
            this.isGetCurrentConditionsByZipLoading.update(() => isGetCurrentConditionsByZipLoading)
        }));
    }

    addLocation() {
        this.store.select(selectLocations).pipe(take(1)).subscribe((locations: string[]) => {
            const location: string = this.zipCode.value;
            if (locations.indexOf(location) === -1) {
                this.store.dispatch(getCurrentConditionsByZip({location, updateCache: true}));
            } else {
                alert(`The zipcode ${location} is already in use. Please choose another one`);
            }
        });
    }

    changeCache() {
        this.store.dispatch(setCacheDuration({
            cacheDuration: this.duration.value
        }))
    }

    ngOnDestroy(): void {
        this.subs.forEach((sub: Subscription) => sub.unsubscribe());
    }

}
